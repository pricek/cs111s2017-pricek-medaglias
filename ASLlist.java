// Kierra Price and Sam Medaglia
// CMPSC 111 Spring 2017
// Final Project
// Date: 5/1/17
//
// Purpose: To allow users to input any letter from the alphabet and receive an image of that letter in American Sign Language. This program is useful in teaching people basic American Sign Language.
//***********************************
import java.util.Scanner;
import java.util.ArrayList;
import java.io.File;
import java.util.List;
import java.io.IOException;


public class ASLlist
{
	//declaration of variables
	private ASLImages one;
	public ASLImages two;
	private ASLImages three;
	private ASLImages four;
	private ASLImages five;
	private ASLImages six;
	private ASLImages seven;
	private ASLImages eight;
	private ASLImages nine;
	private ASLImages ten;
	private ASLImages eleven;
	private ASLImages twelve;
	private ASLImages thirteen;
	private ASLImages fourteen;
	private ASLImages fifteen;
	private ASLImages sixteen;
	private ASLImages seventeen;
	private ASLImages eighteen;
	private ASLImages nineteen;
	private ASLImages twenty;
	private ASLImages twentyone;
	private ASLImages twentytwo;
	private ASLImages twentythree;
	private ASLImages twentyfour;
	private ASLImages twentyfive;
	private ASLImages twentysix;
	


	public ASLlist() throws IOException 
	{
		//where the array starts
		String[] array = new String[25]; 
		 one = new ASLImages("a.jpg");
		 two = new ASLImages("b.jpg");
		 three = new ASLImages("c.jpg");
		 four = new ASLImages("d.jpg");
		 five = new ASLImages("e.jpg");
		 six = new ASLImages("f.jpg");
		 seven = new ASLImages("g.jpg");
		 eight = new ASLImages("h.jpg");
		 nine = new ASLImages("i.jpg");
		 ten = new ASLImages("j.jpg");
		 eleven = new ASLImages("k.jpg");
		 twelve = new ASLImages("l.jpg");
		 thirteen = new ASLImages( "m.jpg");
		 fourteen = new ASLImages("n.jpg");
		 fifteen = new ASLImages("o.jpg");
		 sixteen = new ASLImages("p.jpg");
		 seventeen = new ASLImages("q.jpg");
		 eighteen = new ASLImages("r.jpg");
		 nineteen = new ASLImages("s.jpg");
		 twenty = new ASLImages("t.jpg");
		 twentyone = new ASLImages("u.jpg");
		 twentytwo = new ASLImages("v.jpg");
		 twentythree = new ASLImages("w.jpg");
		 twentyfour = new ASLImages("x.jpg");
		 twentyfive = new ASLImages("y.jpg");
		 twentysix = new ASLImages("z.jpg");

		//array list of the numbers
		List list = new ArrayList();
		list.add(one);
		list.add(two);
		list.add(three);
		list.add(four);
		list.add(five);
		list.add(six);
		list.add(seven);
		list.add(eight);
		list.add(nine);
		list.add(ten);
		list.add(eleven);
		list.add(twelve);
		list.add(thirteen);
		list.add(fourteen);
		list.add(fifteen);
		list.add(sixteen);
		list.add(seventeen);
		list.add(eighteen);
		list.add(nineteen);
		list.add(twenty);
		list.add(twentyone);
		list.add(twentytwo);
		list.add(twentythree);
		list.add(twentyfour);
		list.add(twentyfive);
		list.add(twentysix);
	}

	public void getLetter(String one, String two, String three, String four, String five, String six, String seven, String eight, String nine, String ten, String eleven, String twelve, String thirteen, String fourteen, String fifteen, String sixteen, String seventeen, String eighteen, String nineteen, String twenty, String twentyone, String twentytwo, String twentythree, String twentyfour, String twentyfive, String twentysix) throws Throwable 
	{

	//declares variables
	String fileName = null;
	String userInput = null;

		ASLImages images = new ASLImages(fileName); //should call to ASLImages class
		if (userInput.equals("a.jpg")) { //calls on Main, asks if its equal to one in List, then displays image from Images
		images.DisplayImage(one); //calls on ASLImages to display the letter "a"
		}
		else if (userInput.equals("b.jpg")) {
		images.DisplayImage(two);//calls on ASLImages to display the letter "b"
		}
		else if (userInput.equals("c.jpg")) {
		images.DisplayImage(three);//calls on ASLImages to display the letter "c"
		}
		else if (userInput.equals("d.jpg")) {
		images.DisplayImage(four);
		}
		else if (userInput.equals("e.jpg")) {
		images.DisplayImage(five);
		}
		else if (userInput.equals("f.jpg")) {
		images.DisplayImage(six);
		}
		else if (userInput.equals("g.jpg")) {
		images.DisplayImage(seven);
		}
		else if (userInput.equals("h.jpg")) {
		images.DisplayImage(eight);
		}
		else if (userInput.equals("i.jpg")) {
		images.DisplayImage(nine);
		}
		else if (userInput.equals("j.jpg")) {
		images.DisplayImage(ten);
		}
		else if (userInput.equals("k.jpg")) {
		images.DisplayImage(eleven);
		}
		else if (userInput.equals("l.jpg")) {
		images.DisplayImage(twelve);
		}
		else if (userInput.equals("m.jpg")) {
		images.DisplayImage(thirteen);
		}
		else if (userInput.equals("n.jpg")) {
		images.DisplayImage(fourteen);
		}
		else if (userInput.equals("o.jpg")) {
		images.DisplayImage(fifteen);
		}
		else if (userInput.equals("p.jpg")) {
		images.DisplayImage(twentysix);
		}
		else if (userInput.equals("q.jpg")) {
		images.DisplayImage(sixteen);
		}
		else if (userInput.equals("r.jpg")) {
		images.DisplayImage(seventeen);
		}
		else if (userInput.equals("s.jpg")) {
		images.DisplayImage(eighteen);
		}
		else if (userInput.equals("t.jpg")) {
		images.DisplayImage(nineteen);
		}
		else if (userInput.equals("u.jpg")) {
		images.DisplayImage(twenty);
		}
		else if (userInput.equals("v.jpg")) {
		images.DisplayImage(twentyone);
		}
		else if (userInput.equals("w.jpg")) {
		images.DisplayImage(twentytwo);
		}
		else if (userInput.equals("x.jpg")) {
		images.DisplayImage(twentythree);
		}
		else if (userInput.equals("y.jpg")) {
		images.DisplayImage(twentyfour);
		}
		else if (userInput.equals("z,jpg")) {
		images.DisplayImage(twentyfive);
		}
		else {
		System.out.println("Thanks for using our program!");
		}

	}


}
