import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.io.File;
import java.awt.*;
import javax.swing.*;
import java.io.IOException;

public class ASLImages
{
	BufferedImage image; //declares image

	public ASLImages(String imageFileName) throws IOException
	{
		image = ImageIO.read(new File(imageFileName)); //reads in the jpg file
	}

	public void DisplayImage(String letter) throws Throwable
	{	
		//where and how the file is displayed
		BufferedImage one = ImageIO.read(new File("/home/p/pricek/cs111S2017/cs111s2017-pricek-medaglias/alphabet/a.jpg"));
		BufferedImage two = ImageIO.read(new File("/home/p/pricek/cs111S2017/cs111s2017-pricek-medaglias/alphabet/b.jpg"));
		BufferedImage three = ImageIO.read(new File("/home/p/pricek/cs111S2017/cs111s2017-pricek-medaglias/alphabet/c.jpg"));
		BufferedImage four = ImageIO.read(new File("/home/p/pricek/cs111S2017/cs111s2017-pricek-medaglias/alphabet/d.jpg"));
		BufferedImage five = ImageIO.read(new File("/home/p/pricek/cs111S2017/cs111s2017-pricek-medaglias/alphabet/e.jpg"));
		BufferedImage six = ImageIO.read(new File("/home/p/pricek/cs111S2017/cs111s2017-pricek-medaglias/alphabet/f.jpg"));
		BufferedImage seven = ImageIO.read(new File("/home/p/pricek/cs111S2017/cs111s2017-pricek-medaglias/alphabet/g.jpg"));
		BufferedImage eight = ImageIO.read(new File("/home/p/pricek/cs111S2017/cs111s2017-pricek-medaglias/alphabet/h.jpg"));
		BufferedImage nine = ImageIO.read(new File("/home/p/pricek/cs111S2017/cs111s2017-pricek-medaglias/alphabet/i.jpg"));
		BufferedImage ten = ImageIO.read(new File("/home/p/pricek/cs111S2017/cs111s2017-pricek-medaglias/alphabet/j.jpg"));
		BufferedImage eleven = ImageIO.read(new File("/home/p/pricek/cs111S2017/cs111s2017-pricek-medaglias/alphabet/k.jpg"));
		BufferedImage twelve = ImageIO.read(new File("/home/p/pricek/cs111S2017/cs111s2017-pricek-medaglias/alphabet/l.jpg"));
		BufferedImage thirteen = ImageIO.read(new File("/home/p/pricek/cs111S2017/cs111s2017-pricek-medaglias/alphabet/m.jpg"));
		BufferedImage fourteen = ImageIO.read(new File("/home/p/pricek/cs111S2017/cs111s2017-pricek-medaglias/alphabet/n.jpg"));
		BufferedImage fifteen = ImageIO.read(new File("/home/p/pricek/cs111S2017/cs111s2017-pricek-medaglias/alphabet/o.jpg"));
		BufferedImage sixteen = ImageIO.read(new File("/home/p/pricek/cs111S2017/cs111s2017-pricek-medaglias/alphabet/p.jpg"));
		BufferedImage seventeen = ImageIO.read(new File("/home/p/pricek/cs111S2017/cs111s2017-pricek-medaglias/alphabet/q.jpg"));
		BufferedImage eighteen = ImageIO.read(new File("/home/p/pricek/cs111S2017/cs111s2017-pricek-medaglias/alphabet/r.jpg"));
		BufferedImage nineteen = ImageIO.read(new File("/home/p/pricek/cs111S2017/cs111s2017-pricek-medaglias/alphabet/s.jpg"));
		BufferedImage twenty = ImageIO.read(new File("/home/p/pricek/cs111S2017/cs111s2017-pricek-medaglias/alphabet/t.jpg"));
		BufferedImage twentyone = ImageIO.read(new File("/home/p/pricek/cs111S2017/cs111s2017-pricek-medaglias/alphabet/u.jpg"));
		BufferedImage twentytwo = ImageIO.read(new File("/home/p/pricek/cs111S2017/cs111s2017-pricek-medaglias/alphabet/v.jpg"));
		BufferedImage twentythree = ImageIO.read(new File("/home/p/pricek/cs111S2017/cs111s2017-pricek-medaglias/alphabet/w.jpg"));
		BufferedImage twentyfour = ImageIO.read(new File("/home/p/pricek/cs111S2017/cs111s2017-pricek-medaglias/alphabet/x.jpg"));
		BufferedImage twentyfive = ImageIO.read(new File("/home/p/pricek/cs111S2017/cs111s2017-pricek-medaglias/alphabet/y.jpg"));
		BufferedImage twentysix = ImageIO.read(new File("/home/p/pricek/cs111S2017/cs111s2017-pricek-medaglias/alphabet/z.jpg"));


		JDialog dialog = new JDialog();
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		dialog.setUndecorated(false);
		JLabel label = new JLabel(new ImageIcon(image));
		dialog.add(label);
		dialog.pack();
		dialog.setVisible(true);
	}
}

